'use strict'
var jwt = require('jsonwebtoken')
module.exports = function(app){
    var userList = require('../controllers/userListController')

    app.route('/users')
        .get(userList.listAllUsers)
        .post(userList.createAUser)

    app.route('/users/:userId')
        .get(userList.readAUser)
        .delete(userList.deleteAUser)
        .post(userList.updateAUser)

    app.route('/veris')
        .get(userList.listDataBaseLogin)
        .post(userList.create)
    
    app.post('/gettoken',(req,res)=>{
        //const user = req.params.apiuser
        //console.log(req.body)
        const user = req.body
    
        const token = jwt.sign( {user: user.id} , 'secret_key')
        //console.log("token ")
        //console.log(token)
        res.json({ 
            success: true, 
            token: token 
            }) 
            res.success = true 
    })
}
