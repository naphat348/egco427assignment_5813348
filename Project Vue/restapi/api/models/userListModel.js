'use strict'
var mongoose = require('mongoose')
var Schema = mongoose.Schema

var UserSchema = new Schema({
    contact_Id: {
        type: String,
    },
    firstName: {
        type: String,
        Required: 'Please enter'
    },
    //missed left last_name
    lastName: {
        type: String,
        Required: 'Please enter'
    },
    mobile_No:{
        type: String,
        Required: 'Please enter'
    },
    email: {
        type: String,
        Required: 'Please enter'
    },
    facebook: {
        type: String,
        Required: 'Please enter'
    },
    img: {
        type: String,
        Required: 'Please enter'
    }
})

var verify = new Schema({
    account: {
        type: String,
        Required: 'Please enter'
    },
    pass: {
        type: String,
        Required: 'Please enter'
    }
}) 

module.exports = mongoose.model('Veri',verify)
module.exports = mongoose.model('Users', UserSchema)