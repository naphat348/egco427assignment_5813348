var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')

app = express()
port = 3000
User = require('./api/models/userListModel')
Veri = require('./api/models/userListModel')
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/Userdb', function (error) {
    if (error) throw error
    console.log('Successfully connected')
})
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())

var routes = require('./api/routes/userListRoutes')
routes(app)

app.listen(3000)
console.log('Server start on : ' + port)

