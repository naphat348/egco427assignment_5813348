import Vue from 'vue'
import Router from 'vue-router'
import SuiVue from 'semantic-ui-vue'
import NewUser from '@/components/NewUser'
import UserList from '@/components/UserList'
import UpdateUser from '@/components/UpdateUser'
import Login from '@/components/Login'

Vue.use(Router)
Vue.use(SuiVue)

export default new Router({
  routes: [
    {
      path: '/new',
      name: 'NewUser',
      component: NewUser
    },
    {
      path: '/',
      name:'Login',
      component: Login
    },
    
    {
      path: '/updateuser/:userId',
      name: 'UpdateUser',
      component: UpdateUser
    },
    {
      path: '/user',
      name: 'UserList',
      component: UserList
    },
    {
      path: '/user',
      name: 'UserList',
      component: UserList
    }
  ]
})
